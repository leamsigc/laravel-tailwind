<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <title>Ismael block Home</title>
        <!-- Fonts -->
        <link
            href="https://fonts.googleapis.com/css?family=Bebas+Neue|Montserrat:400,900&display=swap"
            rel="stylesheet"
        />
        <link href="{{ asset('css/app.css') }}" rel="stylesheet" />
    </head>
    <body>
        <main class="bg-gray-800 font-moserrat relative overflow-hiden">
            <header class="h-25 sm:h-34 flex items-center z-30 absolute top-0 left-0 w-full">
                <div class="container mx-auto px-8 flex items-center justify-between">
                    <div class="uppercase text-blue-600 font-black text-5xl">Ismael</div>
                    <div class="flex items-center">
                        <nav class=" font-sans text-blue-800 uppercase text-lg lg:flex items-center hidden">
                            <a href="/about" class=" py-2 px-5 flex"> About</a>
                            <a href="/about" class=" py-2 px-5 flex"> Contact</a>
                        </nav>
                        <button class=" lg:hidden flex flex-col ml-4 cursor-pointer">
                        <span class=" w-6 h-1 bg-gray-500 mb-1"></span>
                        <span class=" w-6 h-1 bg-gray-500 mb-1"></span>
                        <span class=" w-6 h-1 bg-gray-500 mb-1"></span>
                        </button>
                    </div>
                </div>
            </header>
            {{-- Hero container --}}
               <div class="flex relative z-20">
                <div class="container mx-auto px-6 flex relative">
                    <div
                    class="sm:w-1/2 lg:w-2/5 flex flex-col relative z-20 pt-32 lg:pt-48 pb-20 lg:pb-40"
                    >
                        <span class="w-20 h-2 bg-white mb-12"></span>
                        <h1
                            class="font-bebas-neue uppercase text-6xl sm:text-8xl font-black flex flex-col leading-none text-white"
                        >
                            Get git
                            <span class="text-5xl sm:text-7xl">Running</span>
                        </h1>
                        <p class="text-sm sm:text-base text-white">
                            Lorem ipsum dolor sit amet, consectetuer adipiscing
                            elit, sed diam nonummy nibh euismod tincidunt ut
                            laoreet dolore magna aliquam erat volutpat
                        </p>
                        <div class="flex mt-8">
                            <a
                            href="#"
                            class="uppercase py-2 px-5 sm:px-8 rounded-full bg-yellow-500 border-2 border-transparent text-white text-sm mr-4 hover:bg-yellow-400"
                            >
                            Get started
                            </a>
                            <a
                            href="#"
                            class="uppercase py-2 px-5 sm:px-8 rounded-full bg-transparent border-2 border-yellow-500 text-white hover:bg-yellow-500 hover:text-white text-sm"
                            >
                            Read more
                            </a>
                        </div>
                    </div>
                    <div class="hidden sm:block sm:w-1/2 lg:w-5/5 relative">
                        <img
                            class=" absolute top-0 left-0 right-0 bottom-0"
                            src="https://images.unsplash.com/photo-1571388072750-31a921b3d900?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=2648&q=80"

                        />
                    </div>
                </div>
            </div>
        </main>
        <section class=" bg-white font-montserrat relative overflow-hiden ">
            <div class="container mx-auto flex justify-between my-20">
                <div class=" max-w-sm py-4 px-6 bg-white shadow-lg rounded-lg my-20 ">
                    <div class="flex justify-center md:justify-end -mt-16">
                        <img class="w-20 h-20 object-cover rounded-full border-2 border-indigo-500" src="https://images.unsplash.com/photo-1499714608240-22fc6ad53fb2?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=334&q=80">
                    </div>
                    <div>
                        <h2 class="text-gray-800 text-2xl font-semibold">Design Tools</h2>
                        <p class="mt-2 text-sm text-gray-600">Lorem ipsum dolor sit amet consectetur adipisicing elit. Quae dolores deserunt ea doloremque natus error, rerum quas odio quaerat nam ex commodi hic, suscipit in a veritatis pariatur minus consequuntur!</p>
                    </div>
                    <div class="flex justify-end mt-4">
                        <a href="#" class=" text-xs font-medium text-indigo-500">John Doe</a>
                    </div>
                </div>
                <div class="max-w-sm py-4 px-6 bg-white shadow-lg rounded-lg my-20 ">
                    <div class="flex justify-center md:justify-end -mt-16">
                        <img class="w-20 h-20 object-cover rounded-full border-2 border-indigo-500" src="https://images.unsplash.com/photo-1499714608240-22fc6ad53fb2?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=334&q=80">
                    </div>
                    <div>
                        <h2 class="text-gray-800 text-2xl font-semibold">Design Tools</h2>
                        <p class="mt-2 text-sm text-gray-600">Lorem ipsum dolor sit amet consectetur adipisicing elit. Quae dolores deserunt ea doloremque natus error, rerum quas odio quaerat nam ex commodi hic, suscipit in a veritatis pariatur minus consequuntur!</p>
                    </div>
                    <div class="flex justify-end mt-4">
                        <a href="#" class=" text-xs font-medium text-indigo-500">John Doe</a>
                    </div>
                </div>
                <div class="max-w-sm py-4 px-6 bg-white shadow-lg rounded-lg my-20 ">
                    <div class="flex justify-center md:justify-end -mt-16">
                        <img class="w-20 h-20 object-cover rounded-full border-2 border-indigo-500" src="https://images.unsplash.com/photo-1499714608240-22fc6ad53fb2?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=334&q=80">
                    </div>
                    <div>
                        <h2 class="text-gray-800 text-2xl font-semibold">Design Tools</h2>
                        <p class="mt-2 text-sm text-gray-600">Lorem ipsum dolor sit amet consectetur adipisicing elit. Quae dolores deserunt ea doloremque natus error, rerum quas odio quaerat nam ex commodi hic, suscipit in a veritatis pariatur minus consequuntur!</p>
                    </div>
                    <div class="flex justify-end mt-4">
                        <a href="#" class=" text-xs font-medium text-indigo-500">John Doe</a>
                    </div>
                </div>

            </div>
        </section>
    </body>
</html>
