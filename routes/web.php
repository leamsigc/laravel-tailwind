<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/contact', 'Block\PageController@get_contact');

Route::get('/about', 'Block\PageController@get_about');

Route::get('/', 'Block\PageController@get_index');
