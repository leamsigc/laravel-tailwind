<?php

namespace App\Http\Controllers\Block;

use App\Http\Controllers\Controller;


class PageController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Pages  Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the request for the static pages for the application
    | redirecting them to the route request.
    |
    */

    /**
     * Where to redirect users when the first request the main page.
     *
     * @var string
     */
    public function get_index()
    {
        # Process variable data or the route params
        #
        return view('home', ['login' => 'si']);
    }


    /**
     * Where to redirect users when they request the about us page.
     *
     * @var string
     */
    public function get_about()
    {
        $name = 'ismael';
        $last = 'Garcia';
        $full = $name . ' ' . $last;

        #we can pass  variables to our views
        # view('pages/about')->withFullName($full);

        return view('pages/about', ['fullName' => $full]);
    }

    /**
     * Where to redirect users when they request the contact page.
     *
     * @var string
     */
    public function get_contact()
    {
        return view('pages/contact');
    }
}
