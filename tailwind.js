module.exports = {
    purge: [],
    theme: {
        extend: {
            fontSize: {
                "7xl": "5rem",
                "8xl": "6rem"
            },
            fontFamily: {
                montserrat: ["Montserrat"],
                "bebas-neue": ["Bebas neue"]
            },
            height: {
                "3/4": "75%"
            }
        }
    },
    variants: {},
    plugins: []
};
